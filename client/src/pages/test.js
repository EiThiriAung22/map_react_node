import React from 'react'
import GoogleMapReact from 'google-map-react'
import '../assets/styles/map.css';
import LocationPin from '../components/Location_Pin'
import Box from '@mui/material/Box';
import { withScriptjs, withGoogleMap, GoogleMap, Circle } from "react-google-maps";


const AnyReactComponent = ({ text }) => <div>{text}</div>;

export default function SimpleMap() {
  const defaultProps = {
    center: {
      lat:16.8460288,
      lng: 96.17408
    },
    zoom: 11
  };

  return (
    <Box sx={{display:'flex',padding: '20px'}}>
      <Box sx={{ height: '300px',width: '100%'}}>
        <GoogleMapReact
          bootstrapURLKeys={{ key: "AIzaSyB3y6sDP8WmGBpGwkO_yImQaovmJNkg86I" }}
          defaultCenter={defaultProps.center}
          defaultZoom={defaultProps.zoom}
          yesIWantToUseGoogleMapApiInternals
        >
          <AnyReactComponent
            lat={16.84602883}
            lng={96.17408}
            text="My Marker"
          />
        </GoogleMapReact>
      </Box>
    </Box>
  );
}
