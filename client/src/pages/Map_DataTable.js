import React, { useEffect, useState } from "react";
import DeckGL, { WebMercatorViewport } from "deck.gl";
import MapGL from "react-map-gl";
import renderLayers from "../components/Layer";
import Voronoi from "../components/voronoi";
import { csv } from "d3-fetch";
import Box from '@mui/material/Box';
import DateRangeTableWrapper from "../components/Table_Wrapper";


const MAPBOX_TOKEN = "pk.eyJ1Ijoic2hpbWl6dSIsImEiOiJjam95MDBhamYxMjA1M2tyemk2aHMwenp5In0.i2kMIJulhyPLwp3jiLlpsA";
const DATA_URL = './headmap.csv';

export default () => {

  const [data, setData] = useState({});

  useEffect(() => {
    const fetchData = async () => {
      const result = await csv(DATA_URL);
      const points = result.map(function (d) {
        return { position: [+d.longitude, +d.latitude] };
      });
      setData(points);
    };

    fetchData();
  }, []);

  const [viewport, setViewport] = useState(
    new WebMercatorViewport({
      // width: window.innerWidth,
      // height: window.innerHeight,
      longitude: -3.2943888952729092,
      latitude: 53.63605986631115,
      zoom: 6,
      maxZoom: 16,
      pitch: 0,
      bearing: 0
    })
  );

  //resize
  useEffect(() => {
    const handleResize = () => {
      setViewport((v) => {
        return {
          ...v,
          width: '100%' ,
          height: 300
        };
      });
    };
    handleResize();
    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  }, []);

  return (
    <div style={{padding: '20px',display: 'flex',flexDirection: 'column',gap: '20px'}}>
      <Box sx={{ display: 'flex',height: '300px', width: '100%' }}>
        <MapGL
          {...viewport}
          mapStyle={"mapbox://styles/mapbox/light-v9"}
          mapboxApiAccessToken={MAPBOX_TOKEN}
          preventStyleDiffing={false}
          onViewportChange={(v) => setViewport(new WebMercatorViewport(v))}
        >
          <Voronoi viewport={viewport} data={data} />
          <DeckGL
            layers={renderLayers({
              data: data
            })}
            initialViewState={viewport}
            controller={true}
          />
          
        </MapGL>
      </Box>
      <Box sx={{display: 'flex'}}>
        <DateRangeTableWrapper/>
      </Box>
    </div>
  );
};
