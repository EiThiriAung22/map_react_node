import { useEffect, useState, useCallback, useMemo, useRef, useContext } from "react";
import Box from '@mui/material/Box';
import { DataGrid } from '@mui/x-data-grid';
import ExportPDF from './common/Export_PDF';
import {DataTable} from '../data';
import { csv } from "d3-fetch";
const DATA_URL = './headmap.csv';

const dataTable = DataTable;
function DateRangeTableWrapper() {

    const TableHeaders = [
        {
            field: "deviceId",
            headerName: "Device ID",
            type: "number",
            flex: 0.1,
        },
        {
            field: "latitude",
            headerName: "Latitude",
            type: "number",
            flex: 0.1,
        },
        {
            field: "longitude",
            headerName: "Longitude",
            type: "number",
            flex: 0.1,
        },
        {
            field: "pmlevel",
            headerName: "PM Level",
            type: "number",
            flex: 0.1,
        },
        {
            field: "timestamp",
            headerName: "Timestamp",
            type: "number",
            flex: 0.1,
        },
    ];
    const [pageState, setPageState] = useState({
        isLoading: false,
        data: [],
        columns: TableHeaders,
        total: 0,
        page: 1,
        pageSize: 10,
        skip: 0,
    });

    
  useEffect(() => {

    const fetchData = async () => {
      const result = await csv(DATA_URL);
      
    //   const userRows = result.map(function (d) {
    //     console.log("result info",d)
    //   });
      setPageState(old => ({ ...old, data: result, total: result.length }))

    };

    fetchData();
  }, []);


    return (
        <Box sx={{ display: 'flex', width: '100%', flexDirection: 'column', gap: '20px' }}>
            <Box sx={{ display: 'flex', alignItems: 'center',justifyContent: 'right' }}>
                <ExportPDF pageState={pageState.data} />
            </Box>
            <Box sx={{ display: 'flex', width: '100%' }} >
                <DataGrid
                    width={900}
                    autoHeight
                    rows={pageState.data}
                    rowCount={pageState.total}
                    loading={pageState.isLoading}
                    rowsPerPageOptions={[5, 10, 20]}
                    pagination
                    page={pageState.page - 1}
                    pageSize={pageState.pageSize}
                    paginationMode="server"
                    onPageChange={(newPage) => {
                        setPageState(old => ({ ...old, page: newPage + 1, skip: pageState.pageSize * newPage }))
                    }}
                    onPageSizeChange={(newPageSize) => setPageState(old => ({ ...old, pageSize: newPageSize }))}
                    on
                    columns={pageState.columns}
                    sx={{
                        color: 'black',
                        backgroundColor: 'white',
                        '& .css-yrdy0g-MuiDataGrid-columnHeaderRow': {
                            backgroundColor: '#81d1d0'
                        },
                        '& .MuiDataGrid-columnHeaderTitleContainer': {
                            justifyContent: 'center'
                        },
                        '& .MuiDataGrid-cell': {
                            justifyContent: 'center',
                        },
                        '& .MuiDataGrid-iconSeparator': {
                            display: 'none',
                        },
                        '& .css-1jbbcbn-MuiDataGrid-columnHeaderTitle': {
                            fontWeight: 'bold',
                            opacity: '0.7'
                        }
                    }}
                />
            </Box>
        </Box>
    );
}

export default DateRangeTableWrapper;
