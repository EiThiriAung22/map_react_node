
import Button from '@mui/material/Button';

export default function DefaultButton({ btnTitle, clickAction }) {
    return (
        <Button sx={{
            backgroundColor: "#81d1d0", padding: '8px 16px', color: 'black', opacity: '0.8', fontWeight: '400',
            '&:hover': {
                boxShadow: '6px #81d1d0',
                backgroundColor: "#81d1d0"
            },
        }}
            onClick={clickAction} >{btnTitle}</Button>
    );
}
