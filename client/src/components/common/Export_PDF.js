import { createRef ,useContext } from "react";
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import DefaultButton from '../common/button/Default_Button';
import {GetDownloadPDF} from '../../services/Api_Services';
import axios from 'axios';

function ExportPDF({ pageState }) {


    const downloadReport = () => {
       
       GetDownloadPDF(pageState)
       .then(res => {
        const a = document.createElement('a');
        a.href = window.URL.createObjectURL(res.data);
        a.download = `document.pdf`;
        document.body.appendChild(a);
        a.click();
        //a.remove();
       })
       .catch((err) => {
           console.log("error", err)
       });

    }

    return (
        <Box>
            <DefaultButton btnTitle="Export PDF" clickAction={downloadReport}/>
        </Box>
    );
}

export default ExportPDF;
