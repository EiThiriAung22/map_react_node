import logo from './logo.svg';
import './App.css';
import MapDataTable from './pages/Map_DataTable';

function App() {
  return (
    <MapDataTable/>
  );
}

export default App;
