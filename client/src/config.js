export default function getAxiosConfig() {

     const config = {
        headers: {
          Authorization: "Bearer " + user?.idToken,
        },
        responseType: 'blob',
      };
      return config;
    }
    