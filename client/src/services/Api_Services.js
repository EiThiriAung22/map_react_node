import environment from '../environment';
import axios from 'axios';

const PDFApi = axios.create({
    baseURL: environment.api_server,
});

export const GetDownloadPDF = (data) => {

    let url = `/downloadpdf`;

    return PDFApi.post(`${url}`,data,{responseType: 'blob',contentType :"application/pdf"} );
}