export  const DataTable = {
    data: [
        {
            id: 1,
            deviceId: 13322,
            latitude: '53.8265967429941',
            longitude: '-1.9775390625',
            timestamp: 9096,
            pmlevel: 10,
        },
        {
            id: 2,
            deviceId: 334665,
            latitude: '52.522905940278065',
            longitude: '-3.2080078125',
            timestamp: 4459096,
            pmlevel: 12,
        },
        {
            id: 3,
            deviceId: 334665,
            latitude: '52.83595824834852',
            longitude: '-0.81298828125',
            timestamp: 4459096,
            pmlevel: 12,
        }
    ],
    total_count: 3,
};
