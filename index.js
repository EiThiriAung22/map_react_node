
const express = require("express");
const cors = require("cors");
const app = express();

app.use(cors());
app.use(express.json());
app.listen(3030, function () {
  console.log("Hello Server: port 3030");
});

app.post("/downloadpdf", function (req, res) {

  const fs = require("fs");
  const PDFDocument = require("pdfkit-table");
  let doc = new PDFDocument({ margin: 30, size: 'A4' });
  doc.pipe(fs.createWriteStream("./document.pdf"));

  let requestBody = req.body;
  let newTableRow = [];
  requestBody.map((reqB,reqInd)=>{
    newTableRow[reqInd] = [ reqB.deviceId,reqB.latitude,reqB.longitude,reqB.timestamp,reqB.pmlevel]
  })
  const tableArray = {
    headers: ["DeviceID", "Latitude", "Longitude","Timestamp","PM Level"],
    rows: newTableRow,
  };
  doc.table( tableArray, { width: 600 }); 

  doc.pipe(res);

  doc.end();

  return res.status(201).send(res);

});